from django.test import TestCase
from django.urls import resolve
from .views import *

# Create your tests here.
class Tests(TestCase):

    def test_url(self):
        response = self.client.get('/story8')
        self.assertEquals(response.status_code, 301)

    def test_views(self):
        temp = resolve('/story8/')
        self.assertEqual(temp.func, home)