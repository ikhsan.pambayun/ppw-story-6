from django.shortcuts import render, redirect
from django.http import JsonResponse
import requests


def home(request):

    return render(request, 'home.html')

def cari(request):
    arg = request.GET['q']

    data = requests.get('https://www.googleapis.com/books/v1/volumes?q=' + arg)

    context = data.json()

    return JsonResponse(context, safe = False)
