from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('tambahkegiatan/', views.tambahkegiatan, name='tambahkegiatan'),
    path('tambahpeserta/', views.tambahpeserta, name='tambahpeserta'),
]
