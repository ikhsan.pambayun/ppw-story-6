from django.shortcuts import render, redirect
from .models import *
from .forms import KegiatanForm, PesertaForm



def home(request):
    kegiatan = Kegiatan.objects.all()
    peserta = Peserta.objects.all()

    form1 = KegiatanForm()
    form2 = PesertaForm()

    context = {'kegiatan':kegiatan, 'peserta':peserta, 'form1':form1, 'form2':form2}
    return render(request, 'main/home.html', context)

def tambahkegiatan(request):

    if request.method == 'POST':
        form = KegiatanForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/')
    
    return render(request, 'main/home.html')

def tambahpeserta(request):

    if request.method == 'POST':
        print(request.POST)
        form = PesertaForm(request.POST)
        if form.is_valid():
            Peserta.objects.create(kegiatan = Kegiatan.objects.get(id=request.POST['kegiatan']), nama=request.POST['nama'])
            return redirect('/')

    return render(request, 'main/home.html')
