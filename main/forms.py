from django import forms
from . import models

class KegiatanForm(forms.ModelForm):
    class Meta:
        model = models.Kegiatan
        fields = '__all__'

class PesertaForm(forms.ModelForm):
    class Meta:
        model = models.Peserta
        fields = ('nama',)
