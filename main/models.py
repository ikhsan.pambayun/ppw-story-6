from django.db import models

# Create your models here.

class Kegiatan(models.Model):
    nama = models.CharField(max_length=100, null=True)

    def __str__(self):
        return self.nama

class Peserta(models.Model):
    kegiatan = models.ForeignKey(Kegiatan, null=True, on_delete=models.CASCADE)

    nama = models.CharField(max_length=100, null=True)

    def __str__(self):
        return self.nama