from django.test import TestCase, Client
from .views import *
from .models import *


class MainTestCase(TestCase):
    def test_url_is_exist(self):
        response = self.client.get("/")
        self.assertEqual(response.status_code, 200)

    def test_can_save_a_POST_request_kegiatan(self):
        response = self.client.post("/tambahkegiatan/")
        self.assertEqual(response.status_code, 200)

    def test_can_save_a_POST_request_peserta(self):
        response = self.client.post("/tambahpeserta/")
        self.assertEqual(response.status_code, 200)

    def test_model_can_create_kegiatan(self):
        Kegiatan.objects.create(nama="Mencangkul")
        count = Kegiatan.objects.all().count()
        self.assertEqual(count, 1)

    def test_model_can_create_peserta(self):
        kegiatan = Kegiatan.objects.create(nama="Mencangkul")
        Peserta.objects.create(kegiatan=kegiatan, nama="Mencangkul")
        count = Kegiatan.objects.all().count()
        self.assertEqual(count, 1)




# @tag('functional')
# class FunctionalTestCase(LiveServerTestCase):
#     """Base class for functional test cases with selenium."""

#     @classmethod
#     def setUpClass(cls):
#         super().setUpClass()
#         # Change to another webdriver if desired (and update CI accordingly).
#         options = webdriver.chrome.options.Options()
#         # These options are needed for CI with Chromium.
#         options.headless = True  # Disable GUI.
#         options.add_argument('--no-sandbox')
#         options.add_argument('--disable-dev-shm-usage')
#         cls.selenium = webdriver.Chrome(options=options)

#     @classmethod
#     def tearDownClass(cls):
#         cls.selenium.quit()
#         super().tearDownClass()


# class MainFunctionalTestCase(FunctionalTestCase):
#     def test_root_url_exists(self):
#         self.selenium.get(f'{self.live_server_url}/')
#         html = self.selenium.find_element_by_tag_name('html')
#         self.assertNotIn('not found', html.text.lower())
#         self.assertNotIn('error', html.text.lower())
