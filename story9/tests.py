from django.test import TestCase
from django.urls import resolve
from .views import *

# Create your tests here.
class Tests(TestCase):

    def test_views_signup(self):
        response = self.client.get('/story9/signup/')
        self.assertTemplateUsed(response, 'signup.html')

        newresponse = self.client.post('/story9/signup/', data={'username':'ikhsan', 'password1':'bogor080620', 'password2':'bogor080620'})
        self.assertEqual(newresponse.status_code, 302)
        self.assertEqual(newresponse['location'], '/story9/login/')

    def test_views_login(self):
        response = self.client.get('/story9/login/')
        self.assertTemplateUsed(response, 'login.html')

        self.client.post('/story9/signup/', data={'username':'ikhsan', 'password1':'bogor080620', 'password2':'bogor080620'})
        newresponse = self.client.post('/story9/login/', data={'username':'ikhsan', 'password':'bogor080620'})
        self.assertEqual(newresponse.status_code, 302)
        self.assertEqual(newresponse['location'], '/')

        newresponse = self.client.post('/story9/login/', data={'username':'ikhsanasa', 'password':'bogor080620'})
        self.assertEqual(newresponse.status_code, 302)
        self.assertEqual(newresponse['location'], '/story9/login/')