from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages

# Create your views here.
def signup(request):

    form = UserCreationForm()

    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/story9/login/')

    context = {'form':form}

    return render(request, 'signup.html', context)

def loginacc(request):

    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']

        user = authenticate(request, username=username, password=password)

        if user is not None:
            login(request, user)

            return redirect('/')
        
        else:
            messages.info(request, 'Username atau Password salah')
        
        return redirect('/story9/login/')

    return render(request, 'login.html')

def logoutacc(request):
    logout(request)

    return redirect('/')